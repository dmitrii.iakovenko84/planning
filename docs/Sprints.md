## Sprint 01 (2023-04-28 - 2023-05-14)

**Goal:** Implement minimal functionality for time tracking.

**Acceptance Criteria:**
- There should be a website, without authorization for now, that would display approximately the same information as is currently being managed in Excel.

**Plan:**
- Develop API (Swagger) - 2 SP
- Create backend (Spring Boot)
  * Implement controllers, services, and repositories for Goal, Activity & Task - 1 SP
  * Разработать схему Postgres - 0.5 SP
- Create frontend (Vue + Bootstrap)
  * Develop components for Task, Day, Week - 1 SP
  * Develop API - 1 SP
  * Create design - 1 SP
  * Create SPA - 0.5 SP
- Set up production environment 1 SP
Total: 8 SP (17 days)

**Participants:** 
- Dmitrii Iakovenko (Full stack developer)

**Retrospective Notes:**